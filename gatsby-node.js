exports.createPages = async function({ actions, graphql }) {
  // get all news post slugs
  const { data : newsData } = await graphql(`
    query allPostSlugs {
      allContentfulPost {
        nodes {
          slug
        }
      }
    }
  `)
  // get all press room post slugs
  const { data : pressRoomData } = await graphql(`
    query allPressRoomSlugs {
      allContentfulPressRoom {
        nodes {
          slug
        }
      }
    }
  `)

  // generate blog post pages
  newsData.allContentfulPost.nodes.forEach(post => {
    const slug = post.slug
    actions.createPage({
      path: `/news/${slug}`,
      component: require.resolve(`./src/templates/blogPost.js`),
      context: { slug: slug },
    })
  })
  // generate press room post pages
  pressRoomData.allContentfulPressRoom.nodes.forEach(post => {
    const slug = post.slug
    actions.createPage({
      path: `/press-room/${slug}`,
      component: require.resolve(`./src/templates/pressRoomPost.js`),
      context: { slug: slug },
    })
  })
}
