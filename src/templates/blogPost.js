// External libraries
import React from "react"
import { graphql } from "gatsby"
import { Helmet } from 'react-helmet'
import Img from "gatsby-image"
// import { BLOCK } from "@contentful/rich-text-types"
import { documentToReactComponents } from "@contentful/rich-text-react-renderer"

import BackLink from '../components/backLink'
import { blogPostRichTextOptions } from '../../static/utils/richTextOptions/blogPost'

const heroImgStyles = {
  "height": "500px",
  "objectFit": "cover",
  "justifySelf": "center",
  "minWidth": "200px",
}

export default ({ data }) => {
  // query returns array of one post (from unique slug), get it
  const post = data.allContentfulPost.nodes[0]
  // convert Contentful rich text (json) to html, with options
  const content = documentToReactComponents(post.content.json, blogPostRichTextOptions)
  return (
    <>
    <Helmet>
      <title>{`${post.title} | inCORPUS`}</title>
    </Helmet>
    <div className='container'>
      <div className='single-post'>
        <BackLink to="/news" linkText="News" />
        <Img fluid={post.image.fluid} style={{...heroImgStyles}} alt="" />
        <h1>{post.title}</h1>
        <p>{post.publishDate}</p>
        <div className='post-content'>
          {content}
        </div>
        {/* only show author if provided */}
        {post.author && <p>{post.author}</p>}
        <BackLink to="/news" linkText="News" />
      </div>
    </div>
    </>
  )
}

// need to query all posts and filter with unique slug, as opposed to query single 'contentfulPost'
// reason: rich text embedded asset json only avail in allContentPost Query
export const query = graphql`
  query allContentfulPostQuery($slug: String!) {
    allContentfulPost(filter: {slug: {eq: $slug}}) {
      nodes {
        id
        author
        content {
          json
        }
        title
        slug
        publishDate(formatString: "MMMM D, YYYY")
        image {
          fluid {
            srcWebp
            tracedSVG
            srcSetWebp
            srcSet
            src
            base64
            sizes
            aspectRatio
          }
        }
      }
    }
  }
`
