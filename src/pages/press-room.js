// External libraries
import React, { useState } from 'react'
import { Helmet } from 'react-helmet'
import { graphql } from 'gatsby'

import PostItemList from '../components/postItemList'
import CategoryFilterButton from '../components/categoryFilterButton'

// Style
import '../styles/index.scss'

const PressRoomPage = ( { data: { posts } }) => {
  // using React hooks to add state to functional component (i.e. without class)
  const [selectedCategory, setSelectedCategory] = useState('all'); // default filter state: all

  const handleFilterChange = (e) => {
    const checkboxes = [...document.querySelectorAll('.filter-checkbox')]
    // set selected filter to 'all' if all checkboxes unchecked
    const allUnchecked = checkboxes.every(cb=> cb.checked === false)
    if(allUnchecked) {
      setSelectedCategory('all')
    }
    // otherwise filter to selected category
    else {
      setSelectedCategory(e.target.value)
      // uncheck non-selected checkboxes
      checkboxes.forEach(checkbox => { if(checkbox.value !== e.target.value) checkbox.checked = false })
    }
  }

  return (
    <div>
      <Helmet>
        <title>Press Room | inCORPUS</title>
      </Helmet>
      <div className='container'>
        <h1>PressRoom</h1>
        <div className="category-filter-group">
          <CategoryFilterButton
            category={"Communiqués de Presse"}
            icon={'<i class="las la-file-alt"></i>'}
            handleFilterChange={handleFilterChange}
          />
          <CategoryFilterButton
            category={"Dossiers de Presse"}
            icon={'<i class="las la-folder"></i>'}
            handleFilterChange={handleFilterChange}
          />
          <CategoryFilterButton
            category={"Retombées"}
            icon={'<i class="las la-certificate"></i>'}
            handleFilterChange={handleFilterChange}
          />
        </div>
        <PostItemList posts={posts} selectedCategory={selectedCategory} />
      </div>
    </div>
  )
}

export default PressRoomPage;

export const query = graphql`
  query allPressRoomQuery {
    posts: allContentfulPressRoom(sort: {order: DESC, fields: publishDate}) {
      nodes {
        publishDate
        slug
        title
        image {
          fluid(maxWidth: 1200) {
            src
          }
        }
        id
        author
        category
      }
    }
  }
`
