// External libraries
import React from 'react'

// Style
import '../styles/index.scss'

export default () => (
  <div className='page'>
    <div className='container'>
      <h1>Solutions</h1>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos ad deserunt itaque ab qui, nostrum neque iusto doloribus, doloremque ratione iste. Excepturi consequuntur tempore esse, doloremque dicta vero doloribus ipsa!
      </p>
    </div>
  </div>
)
