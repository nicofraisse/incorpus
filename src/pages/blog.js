// External libraries
import React from 'react'
import { Helmet } from 'react-helmet'
import { graphql } from 'gatsby'

import PostItem from '../components/postItem.js'

// Style
import '../styles/index.scss'

export const query = graphql`
  query allPostsQuery {
    posts: allContentfulPost(sort: {order: DESC, fields: publishDate}) {
      nodes {
        publishDate
        slug
        title
        image {
          fluid(maxWidth: 1200) {
            src
          }
        }
        id
        author
      }
    }
  }
`

export default ( { data: { posts } }) => (
  <div>
    <Helmet>
      <title>Blog | inCORPUS</title>
    </Helmet>
    <div className='container'>
      <h1>Blog</h1>
      <div className='posts'>
        {posts.nodes.map(post => (
          <PostItem key={post.id} post={post} domain="/news" />
        ))}
      </div>
    </div>
  </div>
)
