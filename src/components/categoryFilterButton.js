import React from 'react';

const CategoryFilterButton = ({category, icon, handleFilterChange}) => (
  <div key={category} className="filter-label-input">
    <input
      type="checkbox"
      className="filter-checkbox"
      value={category}
      name={category}
      id={`filter-${category}`}
      onChange={handleFilterChange}
    />
    <label htmlFor={`filter-${category}`}>
      <div
        className="category-filter-icon"
        dangerouslySetInnerHTML={{__html: icon}}>
        </div>
      <span className="category-filter-label">{category}</span>
    </label>
  </div>
);

export default CategoryFilterButton;
