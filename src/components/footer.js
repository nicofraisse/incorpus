// External libraries
import React from 'react'
import { Link } from 'gatsby'

// Images
import instagram from '../../static/images/instagram.svg'
import facebook from '../../static/images/facebook.svg'
import twitter from '../../static/images/twitter.svg'
import linkedin from '../../static/images/linkedin.svg'

const Footer = (props) => (
  <div className='footer'>
    <div className='container'>
      <div className='link-group'>
        <p className='link-group-title'>Navigation</p>
        <Link to='/'>
          <p>Accueil</p>
        </Link>
        <Link to='/'>
          <p>Membre</p>
        </Link>
        <Link to='/'>
          <p>Solutions</p>
        </Link>
        <Link to='/'>
          <p>Science</p>
        </Link>
        <Link to='/'>
          <p>Expérience</p>
        </Link>
        <Link to='/'>
          <p>Blog</p>
        </Link>
      </div>

      <div className='link-group'>
        <p className='link-group-title'>Be.Care</p>
        <Link to='/'>
          <p>Vision</p>
        </Link>
        <Link to='/'>
          <p>Protocole</p>
        </Link>
        <Link to='/'>
          <p>Equipe</p>
        </Link>
        <Link to='/'>
          <p>Comité scientifique</p>
        </Link>
        <Link to='/'>
          <p>Partenaires scientifique</p>
        </Link>
        <Link to='/press-room'>
          <p>Press Room</p>
        </Link>
        <Link to='/'>
          <p>Politique de confidentialité</p>
        </Link>
        <Link to='/'>
          <p>Mentions légales</p>
        </Link>
      </div>

      <div className='link-group'>
        <p className='link-group-title'>Rejoignez-nous</p>
        <Link to='/'>
          <p>Testez</p>
        </Link>
        <Link to='/'>
          <p>Newsletter</p>
        </Link>
        <Link to='/'>
          <p>Contact</p>
        </Link>
      </div>

      <div className='link-group'>
        <p className='link-group-title'>Newletter</p>
        <input type='text' placeholder='Votre email' class='field'/>

        <div className='social-media'>
          <Link to='/'>
            <img src={instagram} className='instagram' alt='instagram'/>
          </Link>
          <Link to='/'>
            <img src={twitter} className='twitter' alt='twitter'/>
          </Link>
          <Link to='/'>
            <img src={facebook} className='facebook' alt='facebook'/>
          </Link>
          <Link to='/'>
            <img src={linkedin} className='linkedin' alt='linkedin'/>
          </Link>
        </div>
      </div>
    </div>
  </div>
)

export default Footer;
