import React from 'react';

import PostItem from './postItem'

const PostItemList = ({posts, selectedCategory}) => (
  <div className='posts'>
    {posts.nodes.map(post => {
      if(selectedCategory===post.category || selectedCategory==='all')
      return (
          <PostItem key={post.id} post={post} domain="/press-room" />
        )
    })}
  </div>
);

export default PostItemList;
