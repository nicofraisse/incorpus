import React from 'react';
import { Link } from 'gatsby';

const PostItem = ({post, domain}) => (
  <div className='post-item'>
    <Link to={`${domain}/${post.slug}`}>
      <h1>{post.title}</h1>
      <p>{post.category}</p>
    </Link>
  </div>
);

export default PostItem;
