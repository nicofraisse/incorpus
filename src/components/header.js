// External libraries
import React from 'react'
import { Link } from 'gatsby'

// Images
import logo from '../../static/images/incorpus-logo.svg'
import burgerMenu from '../../static/images/burger-menu.svg'

const Header = (props) => (
  <>
    <div className='header'>
      <Link to='/'>
        <img src={logo} className='logo' alt='inCORPUS'/>
      </Link>

      <div className='nav-links-right'>
        <Link to='/solutions'>
          <span>Solutions</span>
        </Link>
        <Link to='/science'>
          <span>Science</span>
        </Link>
        <Link to='/testimoniaux'>
          <span>Testimoniaux</span>
        </Link>
        <Link to='/blog'>
          <span>Blog</span>
        </Link>
        <Link to='/contact'>
          <span>Testez</span>
        </Link>
        <Link to='/contact'>
          <span>Contact</span>
        </Link>
        <Link to='#'>
          <i class="las la-user-plus"></i>
        </Link>
      </div>
    </div>
    <div className='header-mobile'>
      <Link to='/'>
        <img src={logo} className='logo' alt='inCORPUS'/>
      </Link>
      <label htmlFor="toggle">
        <img src={burgerMenu} className='burger-menu' alt='menu'/>
      </label>
      <input type="checkbox" id="toggle"/>
      <div className="nav-links-mobile">
        <Link to='/solutions'>
          <span>Solutions</span>
        </Link>
        <Link to='/science'>
          <span>Science</span>
        </Link>
        <Link to='/testimoniaux'>
          <span>Testimoniaux</span>
        </Link>
        <Link to='/blog'>
          <span>Blog</span>
        </Link>
        <Link to='/contact'>
          <span>Testez</span>
        </Link>
        <Link to='/contact'>
          <span>Contact</span>
        </Link>
        <Link to='#'>
          <i class="las la-user-plus"></i>
        </Link>
      </div>
    </div>
  </>
)

export default Header;
