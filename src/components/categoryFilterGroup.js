import React from 'react';

import folderIcon from "../../static/icons/folder.svg"

const CategoryFilterGroup = ({categories, handleFilterChange, icons}) => (
  <div className="category-filter-group">
    <div className="filter-label-input">
      <label htmlFor="all">all</label>
      <input
        type="checkbox"
        className="filter-checkbox"
        value="all"
        name="all"
        id="all"
        onChange={handleFilterChange}
      />
    </div>

    {/*loop through all cateogries and create filter checkboxes*/}
    {categories.map(category => {
      return (
        <div key={category} className="filter-label-input">
          <label htmlFor={`filter-${category}`}>
            <CategoryFilterIcon src={folderIcon} alt={category} />
          </label>
          <input
            type="checkbox"
            className="filter-checkbox"
            value={category}
            name={category}
            id={`filter-${category}`}
            onChange={handleFilterChange}
          />
        </div>
      )}
    )}
  </div>
);

export default CategoryFilterGroup;
