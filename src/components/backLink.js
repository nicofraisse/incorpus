import React from 'react'
import { Link } from 'gatsby'

const BackLink = ({ to, linkText }) => (
  <Link to={to}>
    <h4 className="back-link">{'< '}{linkText}</h4>
  </Link>
);

export default BackLink;
