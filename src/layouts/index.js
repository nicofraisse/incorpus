// External libraries
import React from 'react';
import { Helmet } from 'react-helmet'

// Components
import Header from '../components/header'
import Footer from '../components/footer'

const GlobalLayout = ({children}) => (
  <>
    <Helmet>
      <title>inCORPUS</title>
      {/* line awesome icon cdn */}
      <link rel='stylesheet' href='https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css' />
    </Helmet>
    <Header />
    {children}
    <Footer />
  </>
);

export default GlobalLayout;
