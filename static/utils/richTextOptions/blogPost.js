// options for contentful rich text conversion for embedded images
import React from 'react'

const blogPostRichTextOptions = {
  renderNode: {
    "embedded-asset-block": node => {
      const src = node.data.target.fields.file["en-US"].url
      const title = node.data.target.fields.title["en-US"]
      return (
        <div className='post-body-img'>
          <img alt={title} src={src} />
        </div>
      )
    },
    // don't render node this node type
    "asset-hyperlink": node => null,
  },
}

export { blogPostRichTextOptions };
